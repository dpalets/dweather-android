package com.dmytropalets.dweather.model

import android.util.Log
import org.json.*
import kotlin.math.*

class ForecastFetcher {

    fun fetchForecast(link: String) : ArrayList<WeatherItem>? {

        val downloadedObject = ObjectDownloader()
        val fetchedData = downloadedObject.downloadJSONDataFromLink(link)

        val longForecastData: ArrayList<WeatherItem> = ArrayList()


        val forecastJSONObject = JSONObject(fetchedData)

/*        val placeJSONObject = forecastJSONObject.getJSONObject("city")

        val placeName = placeJSONObject.getString("name")
        val lat = placeJSONObject.getJSONObject("coord").getDouble("lat")
        val lon = placeJSONObject.getJSONObject("coord").getDouble("lon")
        val countryCode = placeJSONObject.getString("country")

        val placeDataLink =
            "https://api.timezonedb.com/v2.1/get-time-zone?key=NFZXK8HGLNO6" +
                    "&format=json&by=position&lat=${lat}" +
                    "&lng=${lon}"

        Log.i("MSG", placeDataLink)

        val placeDataObject = ObjectDownloader()
        val placeJSONData = placeDataObject
            .downloadJSONDataFromLink(placeDataLink)

        Log.i("MSG", placeJSONData)

        val placeDataJSONObject = JSONObject(placeJSONData)


         val countryName = placeDataJSONObject.getString("countryName")
         val gmtOffset = placeDataJSONObject.getInt("gmtOffset")
         val TimeZoneName = placeDataJSONObject.getString("zoneName")*/

        val forecastJSONArray: JSONArray = forecastJSONObject.getJSONArray("list")

            for (i in 0 until forecastJSONArray.length()) {

                val forecastJSONItem = forecastJSONArray.getJSONObject(i)
                val forecastItem = WeatherItem()

                forecastItem.timeMilSec = forecastJSONItem.getInt("dt")
                forecastItem.timeString = forecastJSONItem.getString("dt_txt")

                val forecastJSONItemMain = forecastJSONItem.getJSONObject("main")

                with(forecastJSONItemMain) {
                    forecastItem.temp = getDouble("temp").roundToInt()
                    forecastItem.pressure = getDouble("pressure").roundToInt()
                    forecastItem.humidity = getDouble("humidity").roundToInt()
                }

                val forecastJSONItemWeather = forecastJSONItem.
                    getJSONArray("weather").getJSONObject(0)

                with(forecastJSONItemWeather) {
                    forecastItem.main = getString("main")
                    forecastItem.description = getString("description").capitalize()
                    forecastItem.iconId = getString("icon")

                    val iconLink = "https://openweathermap.org/img/w/${forecastItem.iconId}.png"
                    forecastItem.icon = downloadedObject.getIcon(iconLink)
                }

                val forecastJSONItemWind = forecastJSONItem.getJSONObject("wind")
                val wind = Wind()

                with(forecastJSONItemWind) {

                    if (fetchedData.contains("\"deg\":")) {
                        wind.deg = getDouble("deg").roundToInt()}
                    else {wind.deg = 366}
                    wind.speed = getDouble("speed").roundToInt()
                    forecastItem.wind = wind
                }

/*                forecastItem.place.placeName = placeName
                forecastItem.place.timeZoneName = TimeZoneName
                forecastItem.place.countryName = countryName
                forecastItem.place.countryCode = countryCode
                forecastItem.place.coord.lon = lon
                forecastItem.place.coord.lat = lat*/

                forecastItem.message = "Parsed OK"

                longForecastData.add(forecastItem)
            }

        return longForecastData
    }

}