package com.dmytropalets.dweather.controller

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.dmytropalets.dweather.R
import com.dmytropalets.dweather.model.WeatherItem
import kotlinx.android.synthetic.main.activity_long_forecast.*
import kotlinx.android.synthetic.main.activity_main.*

class LongForecastActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_long_forecast)

        longForecastLayout.setBackgroundColor(Color.parseColor("#3394FF"))

//        testForecastView.setText("Here we will display the forecast")
//        val args = intent.getBundleExtra("bundle")
        val placeString = intent.getStringExtra("placeString")
//        val longForecast : Array<WeatherItem> = args.getSerializable("longforecast") as Array<WeatherItem>

        testForecastView.setText("Forecast for $placeString")

    }
}
